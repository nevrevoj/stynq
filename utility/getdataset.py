#!/usr/bin/env python3
from pathlib import Path
import wget

scriptpath = Path(__file__)
projectpath = scriptpath.absolute().parent.parent
trainpath = projectpath.joinpath('download/traindata')

trainurl = 'https://www.python-course.eu/data/mnist/mnist_train.csv'
testurl = 'https://www.python-course.eu/data/mnist/mnist_test.csv'



print('Project: ' +  str(projectpath))

print("Checking for dataset directory")

if trainpath.is_dir():
    print(str(trainpath) + ' already exists, delete directory to re-download')
    quit(1)

print(str(trainpath) + ' not present, creating.')

trainpath.mkdir(parents=True)

if trainpath.is_dir():
    print(str(trainpath) + ' created.')
else:
    print('Big booboo happened... actually this should not happen.')
    quit(666)

print("Downloading datasets from https://www.python-course.eu/neural_network_mnist.php")

print(trainpath.joinpath('mnist_test.csv'))
wget.download(testurl,out=str(trainpath.joinpath('mnist_test.csv')))

print('\n' + str(trainpath.joinpath('mnist_train.csv')))
wget.download(trainurl,out=str(trainpath.joinpath('mnist_train.csv')))

print('\nOK')
quit(0)
