import numpy as np
import cv2

img = cv2.imread('./two28.jpg', cv2.IMREAD_GRAYSCALE)
res = np.zeros((10,1))
res[2] = 1
res = res.reshape((1,10))
img = img.reshape((1,784))
img = np.hstack([img, res])

np.savetxt("two.csv", img, delimiter=",", fmt='%d')
print(img)
