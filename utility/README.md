Utility scripts
===============

genrandtrain.py
---------------
Generates random training for the neural network and prints it to stdout.

getdataset.py
-------------
Downloads training and testing data in csv form.
