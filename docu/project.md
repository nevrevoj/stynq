# PYNQ Neural network 
Semestrální projekt do předmětů NI-EHW/ESW. 

## Cíle

- [x] Zprovoznění PYNQ na desce ZYBO
- [x] Implementace neuronové sítě v C
- [ ] Implementace uživatelské aplikace v Python - 80%
- [ ] Trénink neuronové sítě
- [ ] Implementace akcelerátoru za pomoci HLS
- [ ] Zakomponování akcelerátoru do Python aplikace

## Plán
4 milníky
- [x] 1: Vydefinování zadání, obstarání hardware, seznámení s projektem
  - Poslední issue uzavřeno 21.10, půjčení ZYBO
- [x] 2: Spustitelná C implementace v uživatelské aplikaci
  -  Plán byl do 31.10, realita 12.11
- [ ] 3: Trénink neuronové sítě, funkční C implementace
  - Plán do 28.11
- [ ] 4: Hardwarová akcelerace - Konec semestru
  - Plán do 10.12

## Plán jednotlivých milestonů

|   | Martin Zemánek                         | Ilia Popov                                    | Vojtěch Nevřela              |
|---|----------------------------------------|-----------------------------------------------|------------------------------|
| 1 | půjčení ZYBO, implemetace C neur. sítě | studium Python, začátek uživatelské aplikace  | definice zadání, dokumentace |
| 2 | implementace C neur. sítě              | uživatelská aplikace - integrace C neur. sítě | uživatelské aplikace         |
| 3 | trénink neur. sítě - teorie            | trénink neur. sítě - Python implementace      | FPGA akcelerátor             |
| 4 |                                        | integrace FPGA akcelerátoru                   | FPGA akcelerátor integrace   |