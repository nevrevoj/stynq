Accelerator
===========

Build instructions
------------------

The device was created in Vivado and Vivado HLS 2020.1.

Project device used: xc7z010clg400-1

### Accelerator IP generation
Create new project in Vivado HLS, add neural/include/neural.h and neural/include/neural.c as sources and specify 
n_neural_wrap or n_neural_dummy as top level function. The wrap function is the actual neural network, the dummy variant 
only copies 10 image input values to output.

Synthesize the code and export as standard vivado IP.

### Bitstream generation

Create new project, specify no sources and include constraint file accelerator/constr.xdc. Next, create a custom IP 
repository in a directory of choice usng the IP catalog in vivado. When new IP repository is created, add the 
generated IP .zip file. This file can be found in the hls project in 
<project_root>/<solution_name>/impl/ip/<ip_name>.zip.

When the IP has been imported in the catalog, the block design may be imported. To import,run 
accelerator/block_design.tcl file. The block design will be imported.

A HDL wrapper must be created. right click the block design in the sources panel and click "Create HDL wrapper". 
Do not let vivado manage teh wrapper automagically. Instead when asked, tell Vivado to copy the wrapper into the 
project.

Now generate bitsrteam. The required .bit and .hwh files can be found in 
<project_name>/<project_name>.runs/impl_X/<hdl_wrapper_name>.bit and 
<project_name>/<project_name>.srcs/sources_X/bd/design_X/hw_handoff/design_X.hwh respectively.

Rename both files to a common name. Now they can be used.


