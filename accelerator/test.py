# tento kod fungoval, ale v detailech bez zaruky

import time
import numpy as np
import asyncio
import pynq
import pynq.lib
from pynq.lib import AxiGPIO
from pynq import Overlay
from pynq import Xlnk
from pynq.lib import dma
from pynq import mmio

o = Overlay('dummyFloat.bit')

for i in o.ip_dict:
        print(i)

dma_innerCfg = o.axi_dma_0
dma_outputCfg = o.axi_dma_1
dma_inputVec = o.axi_dma_2
dma_innerBias = o.axi_dma_3
dma_outputBias = o.axi_dma_4
dma_outputVec = o.axi_dma_5


xlnk = Xlnk()
inCfgArray  = xlnk.cma_array(shape=(28*28*10,), dtype=np.float32)
outCfgArray = xlnk.cma_array(shape=(10*10,), dtype=np.float32)
inArray     = xlnk.cma_array(shape=(28*28,), dtype=np.float32)
outArray    = xlnk.cma_array(shape=(10,), dtype=np.float32)
outBiasArray    = xlnk.cma_array(shape=(10,), dtype=np.float32)
inBiasArray    = xlnk.cma_array(shape=(10,), dtype=np.float32)

for i in range(28*28*10):
        inCfgArray[i] = i / (28*28)

for i in range(10*10):
        outCfgArray[i] = i /10

for i in range(28*28):
        inArray[i] = i % 256 + 0.5

for i in range(10):
        inBiasArray[i] = i/2

for i in range(10):
        outBiasArray[i] = i/3


print("inner cfg")
print(inCfgArray)
print("out cfg")
print(outCfgArray)
print("in bias cfg")
print(inBiasArray)
print("out bias cfg")
print(outBiasArray)
print("image input")
print(inArray)
print("weights before calculation")
print(outArray)


acc = mmio.MMIO(0x43c00000, 0x10000)
acc.write(0x00, 1)
time.sleep(0.1)
print("#1:" + str(acc.read(0x4)))
acc.write(0x00, 0)
print("#2:" + str(acc.read(0x4)))

# Enable input arguments
acc.write(0x0010, 0x1f)
# Enable outputsi
acc.write(0x0014, 0x1)

acc.write(0x28, 0x00010007)
print("#3:" + str(acc.read(0x4)))
acc.write(0x240, 99)
print("#4:" + str(acc.read(0x4)))
acc.write(0x28, 0x00020000)
print("#5:" + str(acc.read(0x4)))

dma_innerCfg.sendchannel.transfer(inCfgArray, start=0, nbytes=15680)
dma_innerCfg.sendchannel.wait()
dma_innerCfg.sendchannel.transfer(inCfgArray, start=15680, nbytes=15680)
dma_innerCfg.sendchannel.wait()


dma_outputCfg.sendchannel.transfer(outCfgArray)
dma_innerBias.sendchannel.transfer(inBiasArray)
dma_outputBias.sendchannel.transfer(outBiasArray)
dma_inputVec.sendchannel.transfer(inArray)
dma_outputVec.recvchannel.transfer(outArray)


dma_outputVec.recvchannel.wait()
dma_outputCfg.sendchannel.wait()
dma_innerBias.sendchannel.wait()
dma_outputBias.sendchannel.wait()
dma_inputVec.sendchannel.wait()

print("#6:" + str(acc.read(0x4)))

print("output array after DMA")
print(outArray)
