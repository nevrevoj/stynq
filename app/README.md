neurapp
=======

man page
--------
To view man page, run "man ./docu/neurapp.1"


file formats
------------

### Training file
Training file is a simple line oriented file. 
Each line consists of 4 parts. Layer ID, neuron ID, input ID and weight.

Example:<br>
```
0 0 0 99
0 0 1 98
0 0 2 97
...
0 0 1022 127
0 0 1023 58
0 0 B 58
0 1 0 39
0 1 1 1
...
0 9 1022 0
0 9 1023 28
0 9 B 25
1 0 1 25
...
1 0 9 66
1 1 0 68
...
1 9 9 91
1 9 B 22
```

##### Layer ID
Specifies which layer is being described. 0 signifies the layer closest to the inputs.

If the neural network has n layers, the output layer will have the index of n-1.

##### Neuron ID
Index of neuron within the layer. The "topmost" neuron has the index 0.

##### Input ID / Bias
Number of the input. Also, the index of neuron (or input) it is connected 
to form the previous layer. This field is also used to encode the bias values.
When the represented value is meant to me a bias, the number is replaced by a -1.

##### Weight
Weight assigned to the specified input.

### Output file
Also a simple line oriented file, each line consists of multiple fields.
File name and list of confidence values in ascendig order.

Example:
```
image1.jpg 0 0 1 2 0 0 6 0 9 0
image2.jpg 0 2 1 1 0 0 6 1 8 0
image3.jpg 0 3 0 0 0 0 6 0 9 0
image4.jpg 0 1 1 0 0 0 6 2 6 0
image5.jpg 0 0 0 1 0 0 5 0 9 0
image6.jpg 0 0 0 1 0 1 5 0 9 0
...
```
