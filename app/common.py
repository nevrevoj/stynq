from enum import Enum
import logging

name = 'neurapp'


class Runtime(Enum):
    NONE = 0
    SOFTWARE = 1
    HARDWARE = 2


# App wide configuration
config = {
    'runtime': Runtime.NONE,
    'training_file': '',
    'input_dir': '',
    'output_file': '',
    'c_library': '',
    'overlay': ''
}

# Logging

DEBUG_LEVELV_NUM = 5
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
def debugv(self, message, *args, **kws):
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        # Yes, logger takes its '*args' as 'args'.
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)
logging.Logger.debugv = debugv

log = logging.getLogger(name)
log.setLevel(DEBUG_LEVELV_NUM)
consoleformatter = logging.Formatter("[%(levelname)s] %(message)s")
cl = logging.StreamHandler()
cl.setFormatter(consoleformatter)
log.addHandler(cl)
