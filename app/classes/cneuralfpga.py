from classes.cneural import Neural
from common import log,config,DEBUG_LEVELV_NUM
from os import geteuid
from importlib import import_module
import numpy as np
import time

correctHardware = True
try:
    import pynq
    import pynq.lib
    from pynq.lib import AxiGPIO
    from pynq import Overlay
    from pynq import Xlnk
    from pynq.lib import dma
    from pynq import mmio
    correctHardware = True
    log.info("PYNQ compatible hardware detected OK")
except (ImportError,RuntimeError,AttributeError) as error:
    correctHardware = False


class neuralFPGA(Neural):
    def __init__(self, imgdir, training, outputfile):
        if geteuid() != 0:
            raise RuntimeError("FPGA accelerator must be run as root!")
        if not correctHardware:
            raise RuntimeError('FPGA accelerator can be run only on PYNQ compatible hardware!')
        Neural.__init__(self, imgdir, training, outputfile)

        self.o = Overlay(config['overlay'])

        if log.getEffectiveLevel() <= DEBUG_LEVELV_NUM:
            log.debugv('List of IPs found in design')
            for i in self.o.ip_dict:
                log.debugv(i)

        self.dma_innerCfg =  self.o.axi_dma_0
        self.dma_outputCfg = self.o.axi_dma_1
        self.dma_inputVec =  self.o.axi_dma_2
        self.dma_innerBias = self.o.axi_dma_3
        self.dma_outputBias = self.o.axi_dma_4
        self.dma_outputVec = self.o.axi_dma_5

        self.xlnk = xlnk = Xlnk()

        inCfgArray = xlnk.cma_array(shape=(28 * 28 * 10,), dtype=np.float32)
        outCfgArray = xlnk.cma_array(shape=(10 * 10,), dtype=np.float32)
        inBiasArray = xlnk.cma_array(shape=(10,), dtype=np.float32)
        outBiasArray = xlnk.cma_array(shape=(10,), dtype=np.float32)
        self.inArray = xlnk.cma_array(shape=(28*28,), dtype=np.float32)
        self.outArray = xlnk.cma_array(shape=(10,), dtype=np.float32)

        log.info("-- Accelerator init start --")
        self.acc = mmio.MMIO(0x43c00000, 0x10000)
        self.acc.write(0x00, 1)
        time.sleep(0.1)
        log.debug("ACC Reset: " + str(self.acc.read(0x4)))
        self.acc.write(0x00, 0)
        log.debug("ACC Config: " + str(self.acc.read(0x4)))
        # Enable input arguments
        self.acc.write(0x0010, 0x4)
        # Enable outputsi
        self.acc.write(0x0014, 0x1)
        log.debug("ACC Copy training: " + str(self.acc.read(0x4)))

        inCfgArray[:] = np.array(self.innerConfig)
        outCfgArray[:] = np.array(self.outputConfig)
        # INSERT PROPER BIAS HERE
        inBiasArray[:] = np.array([0,0,0,0,0,0,0,0,0,0])
        outBiasArray[:] = np.array([0,0,0,0,0,0,0,0,0,0])

        #log.debug("len(inCfgArray) = " + str(len(inCfgArray)))
        #log.debug(inCfgArray)
        #log.debug("len(outCfgArray) = " + str(len(outCfgArray)))
        #log.debug(outCfgArray)

        log.debug("ACC Training DMA load: " + str(self.acc.read(0x4)))
        #Send all and first half of config
        self.dma_innerCfg.sendchannel.transfer(inCfgArray,start=0,nbytes=15680)
        self.dma_outputCfg.sendchannel.transfer(outCfgArray)
        self.dma_innerBias.sendchannel.transfer(inBiasArray)
        self.dma_outputBias.sendchannel.transfer(outBiasArray)
        #Wait for all and first half of config
        self.dma_innerCfg.sendchannel.wait()
        self.dma_outputCfg.sendchannel.wait()
        self.dma_innerBias.sendchannel.wait()
        self.dma_outputBias.sendchannel.wait()
        #Send and wait for second half of config
        self.dma_innerCfg.sendchannel.transfer(inCfgArray,start=15680,nbytes=15680)
        self.dma_innerCfg.sendchannel.wait()
        self.log_acc_status("Finished sending config")

        #log.debug("ACC Push input args")
        #self.acc.write(0x28,0x1B)
        #self.acc.write(0x28,0x1)
        log.info("-- Accelerator init done --")

    def run_neural(self):
        if len(self.images) == 0:
            log.warning('No images loaded, neural network will not run')
        else:
            log.info('Running analysis for ' + str(len(self.images)) + ' images')
        for i in self.images:
            self.run_single_neural(i)

    def run_single_neural(self, name):
        self.log_acc_status(name)
        # transform image to array
        self.inArray[:] = self.images[name].reshape((28 * 28,))
        #Send image via DMA
        self.dma_inputVec.sendchannel.transfer(self.inArray)
        self.dma_inputVec.sendchannel.wait();

        #start result channel
        self.dma_outputVec.recvchannel.transfer(self.outArray)

        #start channel
        self.acc.write(0x28,0x00010001)
        self.acc.write(0x240, 99)
        self.acc.write(0x28,0x00020000)

        #clearflags and move image out of fifo
        self.acc.write(0x4,0xF)
        self.acc.write(0x28,0x4)

        self.dma_outputVec.recvchannel.wait()

        self.output[name] = [self.outArray[i] for i in range(10)]
        log.debug('Analysed ' + str(name) + ' -> ' + str(self.output[name]))

    def log_acc_status(self, text):
        if log.getEffectiveLevel() <= DEBUG_LEVELV_NUM:
            log.debugv("----------------------------------------")
            log.debugv("status: " + str(text) + ": " + str(self.acc.read(0x4)))
            log.debugv("iarg0 (inner cfg)  status: " + str(self.acc.read(0x100)))
            log.debugv("iarg1 (out cfg)    status: " + str(self.acc.read(0x104)))
            log.debugv("iarg2 (image)      status: " + str(self.acc.read(0x108)))
            log.debugv("iarg3 (inner bias) status: " + str(self.acc.read(0x10c)))
            log.debugv("iarg4 (out bias)   status: " + str(self.acc.read(0x110)))
            log.debugv("oarg0 (weights)    status: " + str(self.acc.read(0x140)))
            log.debugv("----------------------------------------")

