from classes.cneural import Neural
from ctypes import CDLL, POINTER, ARRAY
from array import *
import ctypes
from common import config, log
import numpy as np

class neuralC(Neural):
    def __init__(self, imgdir, training, outputfile):
        Neural.__init__(self, imgdir, training, outputfile)

        self.neural = CDLL(config['c_library'])
        log.debug('Loaded library ' + config['c_library'])


        # initialize neural
        self.neural.n_initialize.argtypes = [POINTER(ctypes.c_float), POINTER(ctypes.c_float), POINTER(ctypes.c_float), POINTER(ctypes.c_float)]
        self.neural.n_neural.argtypes = [POINTER(ctypes.c_float), ARRAY(ctypes.c_float, 10)]
        self.neural.n_initialize((ctypes.c_float * 7840)(*self.innerConfig), (ctypes.c_float * 100)(*self.outputConfig), (ctypes.c_float * 10)(*self.biasHidenArr), (ctypes.c_float * 10)(*self.biasOutArr))
        log.info('C neural network initialized!')


    def run_single_neural(self, name):
        # transform image to array
        inputImg = self.images[name].reshape((28 * 28, 1))
        castInputImg = (ctypes.c_float * (28 * 28))()

        assert len(castInputImg) == inputImg.nbytes

        ctypes.memmove(ctypes.byref(castInputImg), inputImg.ctypes.data, inputImg.nbytes)

        # run neural

        outImg = (ctypes.c_float * 10)()


        self.neural.n_neural((ctypes.c_float * 784)(*inputImg), outImg)

        # open file self.outputFile, write 10 output values

        self.output[name] = [outImg[i] for i in range(10)]
        log.debug('Analysed ' + str(name) + ' -> ' + str(self.output[name]))

    def run_neural(self):
        if len(self.images) == 0:
            log.warning('No images loaded, neural network will not run')
        else:
            log.info('Running analysis for ' + str(len(self.images)) + ' images')
        for i in self.images:
            self.run_single_neural(i)
