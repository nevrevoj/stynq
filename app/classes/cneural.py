import numpy as np
import cv2
from common import config, log
from pathlib import Path, PurePath


class Neural:
    def __init__(self, imgdir, training, outputfile):
        self.inputImgDir = imgdir
        self.outputFile = outputfile
        self.trainingFile = training

        # data for passing into neural
        self.images = {}  # array of images as read by cv2 (actually is numpy 2d array)
        self.training = {}  # training dictionary to be passed into neural init
        self.output = {}  # output from last calculation

        # Check args for basic properties
        if not isinstance(self.inputImgDir, PurePath) or not self.inputImgDir.is_dir():
            raise RuntimeError('Imgdir is not a directory')
        if not isinstance(self.outputFile, PurePath):
            raise RuntimeError('Output file is not a valid path')
        if not isinstance(self.trainingFile, PurePath) or not self.trainingFile.is_file():
            raise RuntimeError('Training file is not a file')

        self.load_image_dir()
        self.load_training_file()

        self.innerConfig = []
        self.outputConfig = []
        self.biasHidenArr = []
        self.biasOutArr = []

        for x in self.training:
            if x[2] == -1:
                if x[0] == 0:
                    self.biasHidenArr.insert(int(x[1]), self.training[x])
                elif x[0] == 1:
                    self.biasOutArr.insert(int(x[1]), self.training[x])
                else:
                    log.warning('Training bad layer index: ' + str(x))      
            elif x[0] == 0:
                self.innerConfig.insert(int(x[1]), self.training[x])
            elif x[0] == 1:
                self.outputConfig.insert(int(x[2]), self.training[x])
            else:
                log.warning('Training bad layer index: ' + str(x))

    def save_output(self):
        if len(self.output) == 0:
            log.warning('Writing empty output to file. No images provided?')

        with open(self.outputFile, 'w') as f:
            for i in self.output:
                f.write('\"' + str(i) + '\" ')
                for j in self.output[i]:
                    f.write(str(j) + ' ')
                f.write('\n')

    def load_image_dir(self):
        exts = ['.jpg', '.jpeg', '.png']
        files = []

        files = [p for p in Path(self.inputImgDir).iterdir() if p.suffix in exts]

        log.info('Loading ' + str(len(files)) + ' from directory ' + str(self.inputImgDir))
        if len(files) == 0:
            log.warning('Loading directory with no image files.')

        for f in files:
            self.load_single_image(f)

    def load_single_image(self, imagepath):
        if not isinstance(imagepath, PurePath) or not imagepath.is_file():
            log.error('Bad image file provided')
            return False
        img = cv2.imread(str(imagepath.absolute()), cv2.IMREAD_GRAYSCALE)

        # make sure opencv read the img ok
        if img is None:
            log.error('Unable to load image ' + str(imagepath))
            return False

        # check image size
        h, w = img.shape

        if w == 28 and h == 28:
            log.debug('Image \"' + str(imagepath) + '\" OK')
        else:
            log.debug('Image \"' + str(imagepath) + '\" bad size. W=' + str(w) + ' H=' + str(h))
            return False
        # append to array of images
        self.images[imagepath] = img
        return True

    def load_training_file(self):
        if not isinstance(self.trainingFile, PurePath) or not self.trainingFile.is_file():
            log.error('Bad training file provided')
            return False
        linecnt = -1
        with open(self.trainingFile) as fp:
            while True:
                linecnt += 1
                # read a line
                line = fp.readline()
                # check for EOF
                if not line:
                    break
                # split into fields and check count
                splitline = line.split()
                if len(splitline) != 4:
                    log.warning('Training[' + str(linecnt) + '] - bad number of fields')
                    continue
                # convert to ints and check
                intline = []
                try:
                    intline = [float(i) for i in splitline]
                except ValueError:
                    log.warning('Training[' + str(linecnt) + '] - field not an integer')
                    continue
                # try inserting in training dict
                if tuple(intline[0:3]) in self.training:
                    log.warning('Training[' + str(linecnt) + '] - duplicate key')
                    continue
                else:
                    self.training[tuple(intline[0:3])] = intline[3]
                #log.debug('Training[' + str(linecnt) + '] - OK ' + str(tuple(intline[0:3])) + ' <- ' + str(intline[3]))

        # add verification of stored training, for now assuming its ok
        log.info('Training loaded ' + str(len(self.training)) + ' entries')
        return True
