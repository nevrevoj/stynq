from classes.cneural import Neural
from common import log


class neuralDUMMY(Neural):
    def __init__(self, imgdir, training, outputfile):
        Neural.__init__(self, imgdir, training, outputfile)
        log.info('Dummy neural __init__')

    def run_neural(self):
        log.info('Dummy neural run')
