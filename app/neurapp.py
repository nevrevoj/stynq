#!/usr/bin/env python3

import argparse
from common import log, cl, config, Runtime,DEBUG_LEVELV_NUM
import logging
from pathlib import Path
from classes.cneuralc import neuralC
from classes.cneuralfpga import neuralFPGA
from classes.cneuraldummy import neuralDUMMY

ap = argparse.ArgumentParser()
tt = ap.add_mutually_exclusive_group()

ap.add_argument('-v', '--verbosity',
                help='console logging verbosity',
                action='count',
                default=0)
tt.add_argument('-c',
                help='run C implementation of neural network',
                action='store_true')
tt.add_argument('-f',
                help='run FPGA accelerated implementation of neural network - requires root privileges',
                action='store_true')
ap.add_argument('-t',
                help='file with neural network training',
                required=True)
ap.add_argument('-i',
                help='directory containing input images',
                required=True)
ap.add_argument('-o',
                help='output file containing the weights for each number for each image',
                required=True)
ap.add_argument('--nlib',
                help='c neural library',
                type=str,
                default='./libfitneural.so')
ap.add_argument('--overlay',
                help='fpga overlay',
                type=str,
                default='./accelerator.bit')

args = ap.parse_args()

if args.verbosity == 0:
    cl.setLevel(logging.ERROR)
elif args.verbosity == 1:
    cl.setLevel(logging.WARNING)
elif args.verbosity == 2:
    cl.setLevel(logging.INFO)
elif args.verbosity == 3:
    cl.setLevel(logging.DEBUG)
else:
    cl.setLevel(DEBUG_LEVELV_NUM)

if args.c:
    config['runtime'] = Runtime.SOFTWARE
    log.debug('Runtime = SOFTWARE')
elif args.f:
    config['runtime'] = Runtime.HARDWARE
    log.debug('Runtime = HARDWARE')
else:
    log.critical("Neither -c nor -f was specified!")
    quit(2)

config['training_file'] = Path(args.t)
config['input_dir'] = Path(args.i)
config['output_file'] = Path(args.o)

if not config['training_file'].is_file():
    log.critical('Training file: ' + str(args.t) + ' is not a valid file.')
    quit(2)
else:
    log.debug('Training file = ' + str(config['training_file'].resolve()))

if not config['input_dir'].is_dir():
    log.critical('Input directory: ' + str(args.i) + ' is not a valid directory.')
    quit(2)
else:
    log.debug('Input dir = ' + str(config['input_dir'].resolve()))

if config['output_file'].is_file():
    log.critical('Output file: ' + str(args.o) + ' already exists.')
    quit(12)
else:
    log.debug('Output file = ' + str(config['output_file'].resolve()))

config['c_library'] = args.nlib
config['overlay'] = args.overlay

if args.c:
    try:
        n = neuralC(config['input_dir'], config['training_file'], config['output_file'])
    except OSError as e:
        log.critical(str(e))
        quit(20)
    n.run_neural()
    n.save_output()
elif args.f:
    try:
        n = neuralFPGA(config['input_dir'], config['training_file'], config['output_file'])
    except OSError as e:
        log.critical(str(e))
        quit(31)
    except RuntimeError as e:
        log.critical(str(e))
        quit(31)
    n.run_neural()
    n.save_output()
