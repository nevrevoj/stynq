import csv
import numpy as np
import tensorflow as tf

def neural_fc(x):
	return ( 1 + x / ( 1 + abs ( x ) ) ) / 2

tf.compat.v1.enable_eager_execution(config=None, device_policy=None, execution_mode=None)

# nahrani datoveho souboru
with open('two.csv', newline='\n') as csvfile:
    r = csv.reader(csvfile, delimiter=',', quotechar='"')
    d = [d for d in r]
    data = np.asarray(d, dtype=np.uint32)

# extrakce vstupu (x) a extrakce pozadovanych vystupu (y) pro uceni
x_train = data[:,0:784]
y_train = data[:,784:794]

# normalizace vstupu (vystupy jsou uz normalizovane)
#max = np.max(x_train, axis=1)
# maxima nalezena v datech
#print(max)
#print(max.shape)
#print(x_train.shape)
x_train_norm = x_train / 255.0

#print(x_train_norm)
#print(np.max(x_train_norm))

# vytvoreni modelu
model = tf.keras.Sequential()
model.add(tf.keras.layers.Input(shape=(784,)))
model.add(tf.keras.layers.Dense(10, activation=neural_fc))
model.add(tf.keras.layers.Dense(10, activation=neural_fc))

model.summary()

# kompilace modelu
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.5, clipnorm=1 ),
              loss="mean_squared_error", metrics=['accuracy'])

# vlastni uceni
model.fit(x_train_norm, y_train,
          batch_size=len(x_train_norm), epochs=6000, verbose=2)

# ulozit model
model.save('tfmodel.hd5')

# pokud mate ulozeny model, pak zakomentujte uceni a nahrajte si ho z disku
# to lze doporucit pokud ladite generovani vah do vysldneho formatu napr. do C.
#model = tf.keras.models.load_model('tfmodel.hd5')

#
# tisk vah a prahu z modelu
#
print('Hidden layer')
print("  Bias (threshold)")
print(model.layers[0].bias.numpy())
print('  Weights')
print(model.layers[0].weights[0].numpy().astype(int) * 255)


hiden_layer_id = np.zeros((7840,1))
hiden_neur_id = np.zeros((7840,1))
hiden_input_id = np.zeros((7840,1))
hiden_neur_weight = model.layers[0].weights[0].numpy().reshape((7840,1))
hiden_neur_weight = hiden_neur_weight
for x in range(7840):
    hiden_neur_id[x] = x//784
    hiden_input_id[x] = x % 784

hiden_ress_arr = np.hstack([hiden_layer_id, hiden_neur_id])
hiden_ress_arr = np.hstack([hiden_ress_arr, hiden_input_id])
hiden_ress_arr = np.hstack([hiden_ress_arr.astype(int), hiden_neur_weight])


layer_arr = np.zeros((100,1))
neur_id = np.zeros((100,1))
input_id = np.zeros((100,1))
weight_neur = model.layers[1].weights[0].numpy().reshape((100,1))
weight_neur = weight_neur
i=0
for x in range(100):
    neur_id[x] = i//10
    input_id[x] = i % 10
    i = i+1
    layer_arr[x] = 1

ress_arr = np.hstack([layer_arr.astype(int), neur_id.astype(int)])
ress_arr = np.hstack([ress_arr, input_id.astype(int)])
res_arr = np.hstack([ress_arr.astype(int), weight_neur])

final_train = np.vstack([ hiden_ress_arr, res_arr])
np.savetxt("weights.txt", final_train, delimiter=" ", fmt=['%d' ,'%d', '%d', '%f'])
np.savetxt("biasHiden", model.layers[0].bias.numpy(), delimiter=",", fmt='%f')
np.savetxt("biasOut", model.layers[1].bias.numpy(), delimiter=",", fmt='%f')
print('Output layer')
print("  Bias (threshold)")
print(model.layers[1].bias.numpy())
print('  Weights')
print(model.layers[1].weights[0].numpy().reshape((100,1)))