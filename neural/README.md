libfitneural.so
===============

C implementation of a simple neural network. Currently can be compiled into a
.so library.

make
----

- all
  -  Compiles library into .so file
- testclib
  -  Tests if the library can be linked from python
- clean
  -  Removes temporry build files
- mrproper
  -  clean + removes compield lib
  -  returns repository into default state
