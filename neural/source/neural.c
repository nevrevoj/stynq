#include "../include/neural.h"

void n_initialize(N_DTYPE innerConfig[N_INNER_SIZE][N_INPUT_SIZE] ,N_DTYPE outputConfig[N_OUTPUT_SIZE][N_INNER_SIZE], N_DTYPE innerBias[N_INNER_SIZE], N_DTYPE outputBias[N_OUTPUT_SIZE])
{
    int i, j;
    for ( i = 0 ; i < N_INNER_SIZE ; i++ )
    {
        for ( j = 0 ; j < N_INPUT_SIZE ; j++ )
            n_innerConfig[i][j] = innerConfig[i][j];
        
        for ( j = 0 ; j < N_OUTPUT_SIZE ; j++ )
            n_outputConfig[j][i] = outputConfig[j][i];
	n_innerBias[i] = innerBias[i];
	n_outputBias[i] = outputBias[i];
    }
}

N_DTYPE n_hidden_neuron(N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE configVector[N_INPUT_SIZE], int neuronId)
{
    int i;
    N_DTYPE rtr = n_innerBias[neuronId];
    for ( i = 0 ; i < N_INPUT_SIZE ; i++ )
        rtr += ( inputVector[i] * configVector[i]);
    return N_EVAL_FC_00(rtr);
}

N_DTYPE n_output_neuron(N_DTYPE inputVector[N_INNER_SIZE], N_DTYPE configVector[N_INNER_SIZE], int neuronId)
{
    int i;
    N_DTYPE rtr = n_outputBias[neuronId];
    for ( i = 0 ; i < N_INNER_SIZE ; i++ )
        rtr += ( inputVector[i] * configVector[i]);
    return N_EVAL_FC_01(rtr);
}

void n_neural(N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE outputVector[N_OUTPUT_SIZE])
{
    N_DTYPE innerResults [N_INNER_SIZE];
    int i;
    //hidden layer
    for ( i = 0 ; i < N_INNER_SIZE ; i++ )
    {
        //evaluate one inner neuron
        innerResults[i] = n_hidden_neuron( inputVector, n_innerConfig[i], i);
    }
    
    
    //output layer
    for ( i = 0 ; i < N_OUTPUT_SIZE ; i++ )
    {
        //evaluate one output neuron
        outputVector[i] = n_output_neuron( innerResults, n_outputConfig[i], i);
    }
    
}

N_DTYPE n_neuron_eval_function( N_DTYPE x )
{
    //sigmoid approximation
    //based on https://stackoverflow.com/questions/10732027/fast-sigmoid-algorithm
    // x/ (1- |x|) moved and streached over interval <0:N_DTYPE_MAX)
    if ( x > 0 )
        return (( ( x ) / ( 1 + x ) ) + 1) / 2 ;
    else
        return (( ( x ) / ( 1 - x ) ) + 1) / 2 ;
}


void n_neural_wrap(N_DTYPE innerConfig[N_INNER_SIZE][N_INPUT_SIZE] ,N_DTYPE outputConfig[N_OUTPUT_SIZE][N_INNER_SIZE], N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE innerBias[N_INNER_SIZE], N_DTYPE outputBias[N_OUTPUT_SIZE] , N_DTYPE outputVector[N_OUTPUT_SIZE])
{
	#pragma HLS INTERFACE ap_ctrl_chain port = return
	#pragma HLS INTERFACE bram port=innerConfig
	#pragma HLS INTERFACE bram port=outputConfig
	#pragma HLS INTERFACE bram port=inputVector
	#pragma HLS INTERFACE bram port=outputVector
	#pragma HLS INTERFACE bram port=innerBias
	#pragma HLS INTERFACE bram port=outputBias

	#pragma HLS array_map variable=n_innerConfig instance=n_config horizontal
	#pragma HLS array_map variable=n_outputConfig instance=n_config horizontal
	#pragma HLS array_map variable=n_innerBias instance=n_config horizontal
	#pragma HLS array_map variable=n_outputBias instance=n_config horizontal


    	n_initialize( innerConfig, outputConfig, innerBias, outputBias);
	n_neural( inputVector, outputVector );
}

void n_neural_dummy(N_DTYPE innerConfig[N_INNER_SIZE][N_INPUT_SIZE] ,N_DTYPE outputConfig[N_OUTPUT_SIZE][N_INNER_SIZE], N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE innerBias[N_INNER_SIZE], N_DTYPE outputBias[N_OUTPUT_SIZE] , N_DTYPE outputVector[N_OUTPUT_SIZE])
{
	int16_t i;
	#pragma HLS INTERFACE ap_ctrl_chain port = return
	#pragma HLS INTERFACE bram port=innerConfig
	#pragma HLS INTERFACE bram port=outputConfig
	#pragma HLS INTERFACE bram port=inputVector
	#pragma HLS INTERFACE bram port=outputVector
	#pragma HLS INTERFACE bram port=innerBias
	#pragma HLS INTERFACE bram port=outputBias

	#pragma HLS array_map variable=n_innerConfig instance=n_config horizontal
	#pragma HLS array_map variable=n_outputConfig instance=n_config horizontal
	#pragma HLS array_map variable=n_innerBias instance=n_config horizontal
	#pragma HLS array_map variable=n_outputBias instance=n_config horizontal

    	n_initialize( innerConfig, outputConfig, innerBias, outputBias);
    for(i = 0; i < N_OUTPUT_SIZE; ++i)
    {
    	outputVector[i] = inputVector[i];
    }
}

