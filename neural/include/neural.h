#include <stdint.h>


#define N_DTYPE float
#define N_INPUT_SIZE (28*28)
#define N_OUTPUT_SIZE 10
#define N_INNER_SIZE 10


N_DTYPE n_neuron_eval_function( N_DTYPE x );

#define N_EVAL_FC_00(x) n_neuron_eval_function (x)
#define N_EVAL_FC_01(x) n_neuron_eval_function (x)

//private 
N_DTYPE n_innerConfig [N_INNER_SIZE][N_INPUT_SIZE]; //[neuronindex][inputindex]
N_DTYPE n_innerBias[N_INNER_SIZE];
N_DTYPE n_outputConfig [N_OUTPUT_SIZE][N_INNER_SIZE];//[neuronindex][inputindex]
N_DTYPE n_outputBias[N_OUTPUT_SIZE];


//public
void n_initialize(N_DTYPE innerConfig[N_INNER_SIZE][N_INPUT_SIZE] ,N_DTYPE outputConfig[N_OUTPUT_SIZE][N_INNER_SIZE], N_DTYPE innerBias[N_INNER_SIZE], N_DTYPE outputBias[N_OUTPUT_SIZE]);
void n_neural(N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE outputVector[N_OUTPUT_SIZE]);

void n_neural_wrap(N_DTYPE innerConfig[N_INNER_SIZE][N_INPUT_SIZE] ,N_DTYPE outputConfig[N_OUTPUT_SIZE][N_INNER_SIZE], N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE innerBias[N_INNER_SIZE], N_DTYPE outputBias[N_OUTPUT_SIZE] , N_DTYPE outputVector[N_OUTPUT_SIZE]);
void n_neural_dummy(N_DTYPE innerConfig[N_INNER_SIZE][N_INPUT_SIZE] ,N_DTYPE outputConfig[N_OUTPUT_SIZE][N_INNER_SIZE], N_DTYPE inputVector[N_INPUT_SIZE], N_DTYPE innerBias[N_INNER_SIZE], N_DTYPE outputBias[N_OUTPUT_SIZE] , N_DTYPE outputVector[N_OUTPUT_SIZE]);
