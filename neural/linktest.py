#!/usr/bin/python3

from subprocess import Popen, PIPE

try:
    out = Popen(
        args="nm ./libfitneural.so",
        shell=True,
        stdout=PIPE
    ).communicate()[0].decode("utf-8")

    attrs = [
        i.split(" ")[-1].replace("\r", "")
        for i in out.split("\n") if " T " in i
    ]

    from ctypes import CDLL

    functions = [i for i in attrs if hasattr(CDLL("./libfitneural.so"), i)]

    print("Functions contained in library: ")
    print(functions)
except Exception as e:
    print("Something is not right with the library:")
    print(e)
    exit(1)

print("Library seems valid!")
exit(0)
