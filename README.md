# NI-ESW, NI-EHW: [STYNQ] Implementace neuronové sítě pro identifikaci číslic s akcelerací na FPGA

## Řešící
* Vojtěch Nevřela
* Ilia Popov
* Martin Zemánek

## Zadání

Týmový projekt do předmětů NI-EHW/ESW 2020/21. Cílem je naprogramovat jednoduchou neuronovou síť a vyzkoušet si akceleraci v FPGA.

Projekt je implementován na desce ZYBO a využívá projektu PYNQ. 

Bude implementována jednoduchá neuronová síť která bude natrénována na rozpoznávání číslic v obraze.
Dalším krokem bude implementace akcelerátoru pomoci HLS syntézy a integrace se stávájícím programem. Nakonec bude zjištěno zrychlení.

Funkčnost neuronové sítě bude ověřována oproti veřejně dostupné trénovací sadě "The MNIST database of handwritten digits".

## Struktura projektu
```
.
+-- README.md                       - hlavní část dokumentace
+-- accelerator
|   +-- bitstream                   - adresář se zesytetizovanými overlayi
|   +-- hls_ip                      - adresář se zkompilovanými ip jádry
|   +-- README.md                   - návod pro použití
|   +-- block_design.tcl            - blokový design pro Vivado
|   +-- constr.xdc                  - constraing Vivado projektu
|   +-- test.py                     - testovací skript pro komunikaci s akcelerátorem
+-- app                             - implementace zastřešující aplikace
+-- docu                            - prezentace
+-- neural          
|   +-- include                     - hlavičkové soubory
|   +-- source                      - zdrojové soubory
|   +-- Makefile                    - Makefile
|   +-- README.md                   - návod pro použití
|   +-- linktest.py                 - skript testující propojitelnost knihovny do pythonu
+-- trainer
|   +-- neural_training_model.py    - trénovací skript
|   +-- weights_10000_fp            - natrénovaná sada
+-- utility
    +-- README.md                   - popis skriptů
    +-- genrandtrain.py             - generátor náhodného tréninku - použit u vývoje
    +-- getdataset.py               - skript pro stažení obrázků
```


## Postup řešení

* [x] Výběr technologií
* [x] Port Pynq na dostupnou platformu
* [x] Návrh neuronové sítě - topologie, typ neuronu, aktivační funkce
* [x] Implementace neuronové sítěv C
* [x] Implementace Python řídicí aplikace
* [x] Trénínk neuronové sítě v Keras/Tensorflow
* [x] Implementace IP jádra akcelrátoru
* [x] Implementace overlaye pro PYNQ
* [ ] Testování a oveření funkčnosti

# Návrh

![System design](docu/system.png)

## Zvolené technologie

*  Aplikace, "glue code" - Python
*  SW neuronová síť - C, gcc
*  Akcelerátor - Vivado, Vivado HLS 2020.1
*  Hardware - Digilent Zybo
*  Trénink - Python, Tensorflow, Keras

## Zařízení
Projekt PYNQ je původně určen pro zařízení Pynq-Z1 od Digilentu a Pynq-Z2 of TUL. Bohužel ani jezno z zařízení nebylo k dispozici a projekt musel být naportován na dostupné zařízení. Tímto zařízením se stal Digilent Zybo ZYNQ - 7000 Development Board. Na této desce byl zprovozněn jak operační systém pro desku PYNQ tak integrace s FPGA. Více informací o projektu PYNQ je k nalezení [zde](http://www.pynq.io/).

## Uživatelská aplikace
Uživatelské rozhraní je čistě textové. Umožňuje zpracovávat dávky obrázků jak se zapojením SW verze tak verze HW. Aplikace využívá knihoven PYNQ pro komunikaci s periferiemi.

Zdrojový kód aplikace se nachází v [adresáři app](./app). Manuálová stránka se nachází v [adresáři app/docu](./app/docu) a je možné si ji zobrazit příkazem `man neurapp.1`.

Řešič má přístup ke dvěma implementacím NN. Hlavní implementace je implementace vygenerovaná HLS syntézou, ta, vzhledem k nutnosti přístupu přimo k hardwary, vyžaduje spuštění programu uživatelem root. Druhou variantou je implementace v jazyce C. Obě tyto sítě mají společné rozhraní, které umožňují nahrátí obrázků a tréninku a následné vyčtení výstupu, který je zapsán to výstupního souboru. 

## C Implementace neuronové sítě
C Implementace se nachází v [adresáři neural](./neural). Knihovnu `libfitneural.so` je možné vygenerovat příkazem `make`.

## FPGA implementace neuronové sítě
FPGA implementace se nachází v [adresáři neural](./neural) včetně předkompilovaných IP jader a overlayů. Návod je v příslušném README.md.

## Učení
Pro učení a jeho ověření jsme využili knihovny Keras/Tensorflow v jazyce Python, která umožňuje snadnou tvorbu modelů neuronových síťí a jejich učení.

Skript pro učení se nachází v [adresáři neural](./training).

Chtěli jsme, aby neuronová síť vykazovala přesnost detekce alespoň 80%. Pozorováním bylo zjištěno, že této přesnosti je dosaženo okolo 6000 iterace. Čas na trénink ale nebyl doposud tak vysoký, tak jsme se rozhodli jít na 10000 iterací a dosáhli přesnosti 93%. **Tyto přesnosti vychází pouze z trénovacího programu, nikoli z běhu naší aplikace.**

## Návrh sítě
Pro naše účely nebylo nutné implementovat nijak složitou neuronovou síť. Vstupem neuronové sítě jsou obrázky o rozměrech 28x28 pizelů, tj. 784 vstupů. Neuronová sí%t má 10 výstupů, každý koresponduje s jistotou, že poskytnutý vstup odpovádá právě oné číslici (výstup 0 ~ číslo 0, výstup 1 ~ číslo 1 atd.).

Naše neuronová síť sestává ze 2 hustě propojených vrstev (každý neuron je připojen ke všem neuronům/vstupům z vrstvy předchozí) o 10 neuronech. Neuronová síť je dopředná. To znamená, že všechny cesty vedou od vstupu na výstup. Neexistuje žádná cesta zpětná. Standartně se jako aktivační funkce pro neuronové síťe používá funkce sigmoidy. Rozhodli jsme se ale tuto fuknci nevyužít. Tato funkce tradičně využívá pro svůj výpočet exponencielu a měli jsme obavy že by mohlo dojít k vyčerpání prostředků ve FPGA při implementaci akcelerátoru. Byla zvolena funkce ` f(x) = x/(1 + |x|)`. Tato funkce má obdobný průběh, jako zmíněná sigmoida, avšak je výpočetně méně náročná, nevyžaduje použití knihoven (v C funkce exp()) nebo vlastní implementace exponenciace. Jediný rozdíl je, že její obor hodnot je na intervalu <-1,1>. Pro naše účely jsme funkci upravili na interval <0:1>, což vedlo na funkci `f(x) = (1 + (x/(1 + |x|))/2`.

## Váhy a Bias

Soubor pro váhy a biasy je soubor vygenerovaný z učení. Pro naše účely jsme si určili vlastní formát dat. 

Definice formátu se nachází v souboru [README](./app/README.md) v adresáři app.

### Ověření výstupu

Pro ověření výstupu jsme použili uživatelskou aplikaci napsanou v rámci této úlohy. Dle specifikací jsme připravili obrázky které byly předloženy na vstup NN. Výstup byl pak porovnán s očekáváním. V první části bylo třeba ověřit model napsaný v jazyce C, zda se chová dobře. Ve druhé části se ověřovalo, zda funguje správně komunikace s vysyntetizovaným blokem. Dalším krokem testování je porovnání výsledků akcelerátoru a C SW implementace. V součásné verzi tyto výsledky nekorespodují. Nakonec bude třeba změřit zrychlení systému oproti softwarem realizované verzi.

## Výsledky

Při realizaci úlohy jsme narazili na mnohá úskalí. ZYBO se nám podařilo zprovoznit jako plnohodnotnou náhradu podporovaných desek PYNQ. Podařilo se nám vygenerovat blok, který realizuje námi navrženou neurální síť. Narazili jsme na problémy jak při napojení tohoto bloku do systému PYNQ tak při jeho syntéze, které se nám ještě nepodařilo zanalyzovat. Model vytvořený pro učení se nám podařilo zprovoznit. Data z něj se nám podařilo použít v modelu sítě s využitím naší uživatelské aplikace. 